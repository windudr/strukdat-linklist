#include <iostream>
using namespace std;

struct Elemtlist{
    char data;
    Elemtlist* next;
};
typedef Elemtlist* pointer;
typedef pointer List;

void createList(List& head){
    head = NULL;
}

void insertFirst(List& head,pointer p_new){
    if (head==NULL){
        head=p_new;
    }
    else {
        p_new->next=head;
        head=p_new;;
    }
}

void insertLast(List& head,pointer p_new){
    pointer tmp;
    if (head==NULL){
        head = p_new;
    }
    else {
        tmp = head;
        while (tmp->next != NULL){
            tmp=tmp->next;
        }
        tmp->next=p_new;
    }
}

void createElement(pointer& p_new){
    p_new = new Elemtlist;
    cout << "Info : ";
    cin >> p_new->data;
    p_new->next = NULL;
}

void deleteFirst(List& head,pointer& p_del){
    if (head==NULL){
        p_del=NULL;
    }
    else if (head->next==NULL){
        p_del=head;
        head=NULL;
    }
    else{
        p_del=head;
        head=head->next;
        p_del->next = NULL;
    }
}

void deleteLast(List& head,pointer& p_del){
    pointer last,preclast;
    if (head==NULL){
        p_del=NULL;
    }
    else if(head->next==NULL){
        p_del=head;
        head=NULL;
    }
    else{
        last=head;
        preclast=NULL;
        while (last->next!=NULL){
            preclast=last;
            last=last->next;
        }
        p_del = last;
        preclast->next=NULL;
    }
}

void isEmpty (List head){
    if (head==NULL){
        cout << "List kosong!" << endl;
    }
    else {
        cout << "List berisi!" << endl;
    }
}

void findFirst (List head,pointer& tmp){
    if (head==NULL){
        cout << "List kosong!" << endl;
        tmp=head;
    }
    else {
        cout << "Head ada!" << endl;
        tmp=head;
    }
}

void findNext (pointer& tmp){
    if (tmp==NULL){
        cout << "List tidak berisi!";
    }
    else {
        if (tmp->next==NULL){
            cout << "Elemen berikut kosong!" << endl;
            tmp=tmp->next;
        }
        else {
            cout << "Elemen masih ada" << endl;
            tmp=tmp->next;
        }
    }
}

void retrieve (pointer tmp){
    cout << tmp->data << endl;
}

main(){
    List list;
    pointer p,tmp,del;
    char keluar[2];
    createList(list);
    createElement(p);
    insertFirst(list,p);
    createElement(p);
    insertLast(list,p);

    isEmpty(list);
    findFirst(list,tmp);
    retrieve(tmp);
    findNext(tmp);
    retrieve(tmp);

    deleteFirst(list,del);
    findFirst(list,tmp);
    retrieve(tmp);

    cout << "Masukkan huruf apapun untuk keluar" << endl;
    cout << "input:";cin >> keluar;
}